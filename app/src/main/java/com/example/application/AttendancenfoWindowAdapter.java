package com.example.application;

import android.app.Activity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;

public class AttendancenfoWindowAdapter implements GoogleMap.InfoWindowAdapter {
    private final Activity activity;

    public AttendancenfoWindowAdapter(Activity activity) {
        this.activity = activity;
    }

    @Override
    public View getInfoWindow(Marker marker) {
        return null;
    }

    @Override
    public View getInfoContents(Marker marker) {
        View view = activity.getLayoutInflater()
                .inflate(R.layout.info_window, null);

        Attendance attendance = (Attendance) marker.getTag();
        TextView tvDescripcio = view.findViewById(R.id.tvDescripcio);

        tvDescripcio.setText(attendance.getDireccio());

        return view;
    }
}
