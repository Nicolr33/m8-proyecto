package com.example.application.ui.fragments;


import android.content.res.Resources;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModelProviders;

import com.example.application.Attendance;
import com.example.application.AttendancenfoWindowAdapter;
import com.example.application.R;
import com.example.application.SharedViewModel;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

/**
 * A simple {@link Fragment} subclass.
 */
public class MapaFragment extends Fragment {


    public MapaFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_mapa, container, false);

        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager()
                .findFragmentById(R.id.g_map);

        FirebaseAuth auth = FirebaseAuth.getInstance();

        DatabaseReference base = FirebaseDatabase.getInstance().getReference();

        DatabaseReference attendances = base.child("attendances");

        SharedViewModel model = ViewModelProviders.of(getActivity()).get(SharedViewModel.class);

        mapFragment.getMapAsync(map -> {
            map.setMyLocationEnabled(true);

            MutableLiveData<LatLng> currentLatLng = model.getCurrentLatLng();
            LifecycleOwner owner = getViewLifecycleOwner();
            try {
                // Customize the styling of the base map using a JSON object defined
                // in a raw resource file.
                boolean success = map.setMapStyle(
                        MapStyleOptions.loadRawResourceStyle(
                                getActivity(), R.raw.map_style));

                if (!success) {
                    Log.e(null, "Style parsing failed.");
                }
            } catch (Resources.NotFoundException e) {
                Log.e(null, "Can't find style. Error: ", e);
            }
            currentLatLng.observe(owner, latLng -> {
                CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 15);
                map.animateCamera(cameraUpdate);
                currentLatLng.removeObservers(owner);
            });

            attendances.addChildEventListener(new ChildEventListener() {
                @Override
                public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                    Attendance attendance = dataSnapshot.getValue(Attendance.class);

                    LatLng aux = new LatLng(
                            Double.parseDouble(attendance.getLatitud()),
                            Double.parseDouble(attendance.getLongitud())
                    );

                    AttendancenfoWindowAdapter customInfoWindow = new AttendancenfoWindowAdapter(
                            getActivity()
                    );

                    map.addMarker(new MarkerOptions()
                            .title(attendance.getNombre())
                            .snippet(attendance.getDireccio())
                            .position(aux)
                            .icon(BitmapDescriptorFactory.defaultMarker
                                    (BitmapDescriptorFactory.HUE_GREEN)));
                    Marker marker = map.addMarker(new MarkerOptions()
                            .title(attendance.getNombre())
                            .snippet(attendance.getDireccio())
                            .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN))
                            .position(aux));
                    marker.setTag(attendance);
                    map.setInfoWindowAdapter(customInfoWindow);
                }

                @Override
                public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                }

                @Override
                public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {
                }

                @Override
                public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                }
            });
        });


        return view;
    }

}
