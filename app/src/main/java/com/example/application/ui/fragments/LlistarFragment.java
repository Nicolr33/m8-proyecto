package com.example.application.ui.fragments;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.example.application.Attendance;
import com.example.application.R;
import com.firebase.ui.database.FirebaseListAdapter;
import com.firebase.ui.database.FirebaseListOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

/**
 * A simple {@link Fragment} subclass.
 */
public class LlistarFragment extends Fragment {


    public LlistarFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_llistar, container, false);

        FirebaseAuth auth = FirebaseAuth.getInstance();
        DatabaseReference base = FirebaseDatabase.getInstance().getReference();

        DatabaseReference incidencies = base.child("attendances");


        FirebaseListOptions<Attendance> options = new FirebaseListOptions.Builder<Attendance>()
                .setQuery(incidencies, Attendance.class)
                .setLayout(R.layout.lv_incidencies_item)
                .setLifecycleOwner(this)
                .build();


        FirebaseListAdapter<Attendance> adapter = new FirebaseListAdapter<Attendance>(options) {
            @Override
            protected void populateView(View v, Attendance model, int position) {
                TextView txtDescripcio = v.findViewById(R.id.txtDescripcio);
                TextView txtAdreca = v.findViewById(R.id.txtAdreca);

                txtDescripcio.setText(model.getNombre());
                txtAdreca.setText(model.getDireccio());
            }
        };

        ListView lvattendances = view.findViewById(R.id.lvIncidencies);
        lvattendances.setAdapter(adapter);


        return view;
    }

}
