package com.example.application;

public class User {
    private String nombre;
    private String uid;

    public User(String nombre, String uid) {
        this.nombre = nombre;
        this.uid = uid;
    }

    public User() {

    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }
}
